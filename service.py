"""This is template for making a service

"""

# python standard library imports
import os
import base64
import logging
import time
import datetime
import json

# third-party module or package imports
import uvicorn
import cv2
import numpy as np
import pydantic

from fastapi import FastAPI, File, UploadFile, Form, Request
from fastapi.encoders import jsonable_encoder
from typing import Optional, List
from pydantic import BaseModel
from PIL import Image

# code repository sub-package imports
from src import rlogger
from src.rcode import rcode
from src import rtracer


# init general envs
now = datetime.datetime.now()

# load config
with open("config/api.json") as jfile:
    config = json.load(jfile)

SERVICE_IP = config["SERVICE_IP"]
SERVICE_PORT = config["SERVICE_PORT"]
LOG_PATH = config["LOG_PATH"]
VERSION = config["VERSION"]
SERVICE_INFO = config["SERVICE_INFO"]
RETURN_INFO = config["RETURN_INFO"]
# TODO load more config here

# create folder structure
if not os.path.exists(LOG_PATH):
    os.mkdir(LOG_PATH)

# init app
app = FastAPI()

# create logger
log_formatter = logging.Formatter("%(asctime)s %(levelname)s" " %(funcName)s(%(lineno)d) %(message)s")
log_handler = rlogger.BiggerRotatingFileHandler(
    "ali",
    LOG_PATH,
    mode="a",
    maxBytes=2 * 1024 * 1024,
    backupCount=200,
    encoding=None,
    delay=0,
)
log_handler.setFormatter(log_formatter)
log_handler.setLevel(logging.INFO)

logger = logging.getLogger("")
logger.setLevel(logging.INFO)
logger.addHandler(log_handler)

logger.info("INIT LOGGER SUCCESSED")

# define class
class Images(BaseModel):
    data: Optional[List[str]] = pydantic.Field(default=None, example=None, description="List of base64 encoded images")


class PredictData(BaseModel):
    # images: Images
    images: Optional[List[str]] = pydantic.Field(
        default=None, example=None, description="List of base64 encoded images"
    )


# TODO load and warm-up model here


# print app info
print("SERVICE_IP", SERVICE_IP)
print("SERVICE_PORT", SERVICE_PORT)
print("LOG_PATH", LOG_PATH)
print("API READY")

# define routes and functions
@app.post("/predict")
async def predict(request: Request, data: PredictData):
    # init variables
    return_result = rcode(1001)

    try:
        start_time = time.time()
        predicts = []
        try:
            images = jsonable_encoder(data.images)
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = rcode(609)
            return

        # Request tracing
        traceid = request.headers.get("rtraceid")
        if traceid is None or traceid == "":
            traceid = rtracer.get_traceid()
        logger.info("trace_id: %s", traceid)

        for image in images:
            image_decoded = base64.b64decode(image)
            jpg_as_np = np.frombuffer(image_decoded, dtype=np.uint8)
            process_image = cv2.imdecode(jpg_as_np, flags=1)
            # uncomment for convert opencv img to pillow img
            # process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
            # process_image = Image.fromarray(process_image)

        # TODO add processing here

        return_result = {
            **rcode(1000),
            **{
                "traceid": traceid,
                "predicts": predicts,
                "process_time": time.time() - start_time,
                "return": "base64 encoded file",
            },
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = rcode(1001)
    finally:
        return return_result


@app.post("/predict_binary")
async def predict_binary(request: Request, binary_file: UploadFile = File(...)):
    # init variables
    return_result = rcode(1001)

    try:
        start_time = time.time()
        predicts = []
        try:
            bytes_file = await binary_file.read()
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = rcode(609)
            return

        # Request tracing
        traceid = request.headers.get("rtraceid")
        if traceid is None or traceid == "":
            traceid = rtracer.get_traceid()
        logger.info("trace_id: %s", traceid)

        # Processing
        nparr = np.fromstring(bytes_file, np.uint8)
        process_image = cv2.imdecode(nparr, flags=1)
        # uncomment for convert opencv img to pillow img
        # process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
        # process_image = Image.fromarray(process_image)

        # TODO add processing here

        return_result = {
            **rcode(1000),
            **{
                "traceid": traceid,
                "predicts": predicts,
                "process_time": time.time() - start_time,
                "return": "base64 encoded file",
            },
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = rcode(1001)
    finally:
        return return_result


@app.post("/predict_multi_binary")
async def predict_multi_binary(request: Request, binary_files: Optional[List[UploadFile]] = File(None)):
    # init variables
    return_result = rcode(1001)

    try:
        start_time = time.time()
        predicts = []
        try:
            bytes_file_list = []
            for binary_file in binary_files:
                bytes_file_list.append(await binary_file.read())
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = rcode(609)
            return

        # Request tracing
        traceid = request.headers.get("rtraceid")
        if traceid is None or traceid == "":
            traceid = rtracer.get_traceid()
        logger.info("trace_id: %s", traceid)

        # Processing
        process_image_list = []
        for bytes_file in bytes_file_list:
            nparr = np.fromstring(bytes_file, np.uint8)
            process_image = cv2.imdecode(nparr, flags=1)
            # uncomment for convert opencv img to pillow img
            # process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
            # process_image = Image.fromarray(process_image)
            process_image_list.append(process_image)

        # TODO add processing here

        return_result = {
            **rcode(1000),
            **{
                "traceid": traceid,
                "predicts": predicts,
                "process_time": time.time() - start_time,
                "return": "base64 encoded file",
            },
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = rcode(1001)
    finally:
        return return_result


@app.post("/predict_multipart")
async def predict_multipart(
    request: Request,
    argument: Optional[float] = Form(...),
    binary_file: UploadFile = File(...),
):
    # init variables
    return_result = rcode(1001)

    try:
        start_time = time.time()
        predicts = []
        try:
            bytes_file = await binary_file.read()
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = rcode(609)
            return

        # Request tracing
        traceid = request.headers.get("rtraceid")
        if traceid is None or traceid == "":
            traceid = rtracer.get_traceid()
        logger.info("trace_id: %s", traceid)

        # Processing
        nparr = np.fromstring(bytes_file, np.uint8)
        process_image = cv2.imdecode(nparr, flags=1)
        # uncomment for convert opencv img to pillow img
        # process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
        # process_image = Image.fromarray(process_image)

        # TODO add processing here

        return_result = {
            **rcode(1000),
            **{
                "traceid": traceid,
                "predicts": predicts,
                "process_time": time.time() - start_time,
                "return": "base64 encoded file",
            },
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = rcode(1001)
    finally:
        return return_result


@app.get("/info")
async def info(request: Request):
    logger.info("info")
    return_result = rcode(1001)

    try:
        start_time = time.time()

        # Request tracing
        traceid = request.headers.get("rtraceid")
        if traceid is None or traceid == "":
            traceid = rtracer.get_traceid()
        logger.info("trace_id: %s", traceid)

        return_result = {
            **rcode(1000),
            **{
                "traceid": traceid,
                "info": SERVICE_INFO,
                "version": VERSION,
                "process_time": time.time() - start_time,
                "return": RETURN_INFO,
            },
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = rcode(1001)
    finally:
        return return_result


# run app
if __name__ == "__main__":
    uvicorn.run(app, port=SERVICE_PORT, host=SERVICE_IP)
